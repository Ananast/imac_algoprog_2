#include "tp4.h"
#include "mainwindow.h"

#include <QApplication>
#include <time.h>
#include <stdio.h>

MainWindow* w = nullptr;
using std::size_t;
using std::string;

int Heap::leftChild(int nodeIndex)
{
    int nodeLeftChild = nodeIndex*2+1;
    return nodeLeftChild;
}

int Heap::rightChild(int nodeIndex)
{
    int nodeRightChild = nodeIndex*2+2;
    return nodeRightChild;
}

void Heap::insertHeapNode(int heapSize, int value)
{
    //Lorsqu’on insère un noeud dans le tas,
    // on doit d’abord compléter le noeud h − 1 incomplet le plus à droite.
	// use (*this)[i] or this->get(i) to get a value at index i
	int i = heapSize;
    this->set(i, value);
    //this->get(i)=value;

    while(i>0 && this->get(i)>this->get((i-1)/2)){
        this->swap(i, (i-1)/2);
        i = (i-1)/2;
    }


}

void Heap::heapify(int heapSize, int nodeIndex)
{
	// use (*this)[i] or this->get(i) to get a value at index i
    int i = nodeIndex;
	int i_max = nodeIndex;
    if(this->leftChild(nodeIndex)<heapSize && this->get(i_max) < this->get(this->leftChild(nodeIndex))){
        i_max = this->leftChild(nodeIndex);
    }
    if(this->rightChild(nodeIndex)<heapSize && this->get(i_max) < this->get(this->rightChild(nodeIndex))){
        i_max = this->rightChild(nodeIndex);
    }

        if(i_max != i){
           this->swap(i, i_max);
           this->heapify(heapSize, i_max);
        }
}

void Heap::buildHeap(Array& numbers)
{
    int size = numbers.size();
    for (int i=0; i< size; i++){
    this->insertHeapNode(i, numbers[i]);
    }
}

void Heap::heapSort()
{
    for (int i = this->size()-1; i>0; i--){
    this->swap(0, i);
    this->heapify(i, 0);
}


}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    MainWindow::instruction_duration = 50;
    w = new HeapWindow();
	w->show();

	return a.exec();
}
