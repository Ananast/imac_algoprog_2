#include "tp1.h"
#include <QApplication>
#include <time.h>
#include <math.h>

int isMandelbrot(Point z, int n, Point point){

    float x = z.x; //partie réelle de z
    float y = z.y; //partie imaginaire de z
    float a = point.x; //partie réelle de point
    float b = point.y; //partie imaginaire de point

    Point next;
      next.x = pow(x,2) - pow(y,2) + a;
      next.y=2*x*y+b;


    float module = sqrt(pow(x,2)+pow(y,2));

    if(n==0){
        return 0;
    }
    else{
        if(module>2){
            return n;
        }
        else{
            return isMandelbrot(next, n-1, point);
        }
    }
    return 0;
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow* w = new MandelbrotWindow(isMandelbrot);
    w->show();

    a.exec();
}



