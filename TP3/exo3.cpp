#include "mainwindow.h"
#include "tp3.h"
#include <QApplication>
#include <time.h>
#include <stack>
#include <queue>

MainWindow* w = nullptr;
using std::size_t;

struct BinarySearchTree : public BinaryTree
{    
    Node* left; //enfant de gauche
    Node* right; //enfant de droite
    int value; //noeud courant

    void initNode(int value)
    {
        //initialisation
        this->left=NULL;
        this->right=NULL;
        this->value = value;
    }

    void insertNumber(int value) {
        //créer un noeud et insérer à droite ou à gauche

        //si la value est plus petite que le noeud
        if(this->value<value){
            //on vérigie s'il existe noeud, s'il existe un noeud
            //si non, on en insère un
            if(this->right == NULL){
                Node* right = createNode(value);
                this->right = right;
            }
            //si oui, on va d'un étage, on passe au noeud suivant
            else{
                //je réappelle la fonction depuis mon noeud courant
                this->right->insertNumber(value);
            }
        }
        //si la value est plus grande que le noeud
        else{
            if(this->left == NULL){
                Node* left = createNode(value);
                this->left = left;
            }
            else{
                this->left->insertNumber(value);
            }
        }
    }

    uint height() const	{
        // should return the maximum height between left child and
        // right child +1 for itself. If there is no child, return
        // just 1
        //initialiser à 1 la taille
        int left_child_height = 1;
        int right_child_height = 1;
        //je m'arrete si le noeud d'après est nul
        if(this->right != NULL){
            //je réappelle la fonction qui va ajouter un +1
            right_child_height = right_child_height + this->right->height();
        }
        if(this->left != NULL){
            left_child_height = left_child_height + this->left->height();
        }

        //Je prends la hauteur la plus grande
        if(left_child_height < right_child_height){
            return right_child_height;
        }
        else{
            return left_child_height;
        }
    }

    uint nodesCount() const {
        // should return the sum of nodes within left child and
        // right child +1 for itself. If there is no child, return
        // just 1
        //il y a au moins 1 noeud, la racine
        int nb_noeud =1;
        if(this->right != NULL){
            //je réappelle la fonction qui va ajouter un +1
            nb_noeud = nb_noeud + this->right->nodesCount();
        }
        if(this->left != NULL){
            //je réappelle la fonction qui va ajouter un +1
            nb_noeud = nb_noeud + this->left->nodesCount();
        }
        return nb_noeud;

    }

    bool isLeaf() const {
        // return True if the node is a leaf (it has no children)
        if(this->left == NULL && this->right == NULL){
            return true;
        }
        else{
            return false;
        }
    }

    void allLeaves(Node* leaves[], uint& leavesCount) {
        // fill leaves array with all leaves of this tree
        //si le noeud courant est une feuille
        if(this->isLeaf()){
            leaves[leavesCount]=this;
            leavesCount ++;
        }
        else{
            if(this->right !=NULL){
                this->right->allLeaves(leaves,leavesCount);
            }
            if(this->left !=NULL){
                this->left->allLeaves(leaves,leavesCount);
            }
        }
    }

    void inorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with inorder travel
        // fils gauche -> parent -> fils droit
        if(this->left !=NULL){
            this->left->inorderTravel(nodes,nodesCount);
        }
        nodes[nodesCount] = this;
        nodesCount++;
        if(this->right !=NULL){
            this->right->inorderTravel(nodes,nodesCount);
        }
    }

    void preorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with preorder travel
        //parent -> fils gauche -> fils droit
        nodes[nodesCount] = this;
        nodesCount++;
        if(this->left !=NULL){
            this->left->preorderTravel(nodes,nodesCount);
        }
        if(this->right !=NULL){
            this->right->preorderTravel(nodes,nodesCount);
        }
    }

    void postorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with postorder travel
        //fils gauche -> fils droit -> parent
        if(this->left !=NULL){
            this->left->postorderTravel(nodes,nodesCount);
        }
        if(this->right !=NULL){
            this->right->postorderTravel(nodes,nodesCount);
        }
        nodes[nodesCount] = this;
        nodesCount++;
    }

    Node* find(int value) {
        // find the node containing value


        if(this->value == value){
            return this;
        }
        else if(this->value < value){
            return this->right->find(value);
        }

        else{
            return this->left->find(value);
        }

        return nullptr;
    }

    void reset()
    {
        if (left != NULL)
            delete left;
        if (right != NULL)
            delete right;
        left = right = NULL;
    }

    BinarySearchTree(int value) : BinaryTree(value) {initNode(value);}
    ~BinarySearchTree() {}
    int get_value() const {return value;}
    Node* get_left_child() const {return left;}
    Node* get_right_child() const {return right;}
};

Node* createNode(int value) {
    return new BinarySearchTree(value);
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow::instruction_duration = 200;
    w = new BinarySearchTreeWindow<BinarySearchTree>();
    w->show();

    return a.exec();
}
